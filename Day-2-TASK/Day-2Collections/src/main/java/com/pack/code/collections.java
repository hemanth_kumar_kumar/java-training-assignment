package com.pack.code;

import java.util.*;

public class collections {
    static Scanner numbers = new Scanner(System.in);

    public static void main(String args[]) {
        collections object = new collections();
        int counter = 0;
        while (counter != 15) {
            System.out.print("enter the any problem number from 1 to 14 to see answer: ");
            counter = numbers.nextInt();
            switch (counter) {
                case 1:
                    object.problem1();
                    System.out.println("\n");
                    break;
                case 2:
                    object.problem2();
                    System.out.println("\n");
                    break;
                case 3:
                    object.problem3();
                    System.out.println("\n");
                    break;
                case 4:
                    object.problem4();
                    System.out.println("\n");
                    break;
                case 5:
                    object.problem5();
                    System.out.println("\n");
                    break;
                case 6:
                    object.problem6();
                    System.out.println("\n");
                    break;
                case 7:
                    object.problem7();
                    System.out.println("\n");
                    break;
                case 8:
                    object.problem8();
                    System.out.println("\n");
                    break;
                case 9:
                    object.problem9();
                    System.out.println("\n");
                    break;
                case 10:
                    object.problem10();
                    System.out.println("\n");
                    break;
                case 11:
                    object.problem11();
                    System.out.println("\n");
                    break;
                case 12:
                    object.problem12();
                    System.out.println("\n");
                    break;
                case 13:
                    object.problem13();
                    System.out.println("\n");
                    break;
                case 14:
                    object.problem14();
                    System.out.println("\n");
                    break;
                case 15:
                    System.out.print("program stopped!!!");
                    break;
                default:
                    System.out.println("enter the wrong choice");
                    break;
            }
        }
    }

    public static List<Integer> datas() {
        List<Integer> values = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        return values;
    }

    public void problem1() {
        int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        System.out.print("enter the value to find in the array");
        int value = numbers.nextInt();
        System.out.print("enter the value to update in the array:");
        int updated = numbers.nextInt();

        for (int i = 0; i < a.length; i++) {
            if (a[i] == value) {
                a[i] = updated;
                break;
            }
        }

        System.out.print("new updated array sequence: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }

    }

    public void problem2() {
        ArrayList<Integer> search = new ArrayList<>();
        search.addAll(datas());
        System.out.println("enter the value need to be search: ");
        int value = numbers.nextInt();
        System.out.println("value present in the arraylist: " + search.contains(value));
    }

    public void problem3() {
        ArrayList<Integer> values = new ArrayList<>();
        values.addAll(datas());
        ArrayList<Integer> copydata = values;
        copydata.forEach(System.out::print);
    }

    public void problem4() {
        LinkedList<Integer> data = new LinkedList<>();
        data.addAll(datas());
        ListIterator<Integer> obj = data.listIterator(data.size());
        System.out.print("reverse values: ");
        while (obj.hasPrevious()) {
            System.out.print(obj.previous() + " ");
        }
    }

    public void problem5() {
        LinkedList<Integer> data = new LinkedList<>();
        data.addAll(datas());
        System.out.println("before inserting the data list view: ");
        data.forEach(number -> System.out.print(number + " "));
        System.out.println("enter how many values need to be updated in the list: ");
        int size = numbers.nextInt();
        for (int i = 0; i < size; i++) {
            System.out.print("enter the index value first and then number: ");
            int index = numbers.nextInt();
            int value = numbers.nextInt();
            data.add(index, value);
        }
        System.out.println("lnkedlist after updating new values");
        data.forEach(number -> System.out.print(number + " "));
    }

    public void problem6() {
        LinkedList<Integer> data = new LinkedList<>();
        data.addAll(datas());
        System.out.println("before shuffling the data list view: ");
        data.forEach(number -> System.out.print(number + " "));
        Collections.shuffle(data);
        System.out.println("after shuffling the data list view:  ");
        data.forEach(number -> System.out.print(number + " "));
    }

    private void problem7() {
        Set<Integer> object2 = new HashSet<>();
        object2.addAll(datas());
        TreeSet<Integer> value = new TreeSet<>(object2);
        value.forEach(data -> System.out.print(data + " "));
    }

    public void problem8() {
        TreeSet<Integer> object = new TreeSet<>();
        object.addAll(datas());
        TreeSet<Integer> object1 = new TreeSet<>();
        object.addAll(datas());
        Boolean value = object.equals(object1);
        System.out.print("Two tree sets equals: " + value);
    }

    public void problem9() {
        TreeSet<Integer> object = new TreeSet<>();
        object.addAll(datas());
        System.out.print("enter the element: ");
        int value = numbers.nextInt();
        object.stream().filter(data -> data < value).forEach(data -> System.out.print(data + " "));
    }

    public void problem12() {
        Map<Integer, String> object = new HashMap();
        object.put(1, "Golang");
        object.put(2, "Java-8");
        object.put(3, "Scala");
        object.put(4, ".Net");
        object.put(5, "Kotlin");

        System.out.print("enter the key value: ");
        int KEY = numbers.nextInt();
        object.entrySet().stream().filter(data -> data.getKey() > KEY)
                .forEach(data -> System.out.println("KEY: " + data.getKey() + "," + "VALUE: " + data.getValue()));

    }

    public void problem13() {
        Map<Integer, String> object = new TreeMap(Collections.reverseOrder());
        object.put(2, "Golang");
        object.put(3, "Java-8");
        object.put(5, "Scala");
        object.put(1, ".Net");
        object.put(4, "Kotlin");
        object.entrySet().stream()
                .forEach(data -> System.out.println("KEY: " + data.getKey() + "," + "VALUE: " + data.getValue()));
    }

    public void problem14() {
        Map<Integer, String> object = new HashMap();
        object.put(2, "Golang");
        object.put(3, "Java-8");
        object.put(5, "Scala");
        object.put(1, ".Net");
        object.put(4, "Kotlin");
        object.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey))
                .forEach(data -> System.out.println("KEY: " + data.getKey() + "," + "VALUE: " + data.getValue()));
    }

    public void problem10() {
        PriorityQueue<Integer> object = new PriorityQueue<>();
        object.addAll(datas());

        Object[] array = object.toArray();
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public void problem11() {
        Map<Integer, String> object = new HashMap();
        object.put(1, "Golang");
        object.put(2, "Java-8");
        object.put(3, "Scala");
        object.put(4, ".Net");
        object.put(5, "Kotlin");

        System.out.print("Map is empty or not: " + object.isEmpty());

    }
}
