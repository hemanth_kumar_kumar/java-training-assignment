package com.pack.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pack.model.Details;

/**
 * Servlet implementation class Form
 */
@WebServlet("/Form")
public class Form extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Form() {
        super();
        // TODO Auto-generated constructor stub
    }
    private com.pack.Datalayer.jdbcconnection employeeDao;

    public void init() {
        employeeDao = new com.pack.Datalayer.jdbcconnection();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 String firstName = request.getParameter("firstName");
	        String lastName = request.getParameter("lastName");
	        String age = request.getParameter("age");
	        String dob = request.getParameter("DOB");
	        String gender = request.getParameter("gender");
	        String Degree= request.getParameter("degree");
            String salary= request.getParameter("sal") ;
            String number=request.getParameter("number");
            Details employee = new Details();
	        employee.setFirstname(firstName);
	        employee.setLastname(lastName);
	        employee.setAge(age);
	        employee.setDOB(dob);
	        employee.setGender(gender);
	        employee.setDegree(Degree);
	        employee.setSalary(salary);
	        employee.setPhonenumber(number);
              int value=0;
	        try {
	           value= employeeDao.registerEmployee(employee);
	        } catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        if(value ==1)
	        {	
	        response.sendRedirect("success.jsp");
	        }
	        else
	        {
	        	response.sendRedirect("error.jsp");
	        }
	      
		doGet(request, response);
	}

}
