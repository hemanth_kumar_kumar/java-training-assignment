package com.pack.Datalayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.pack.model.Details;

public class jdbcconnection {
	public int registerEmployee(Details employee) throws ClassNotFoundException {
        String INSERT_USERS_SQL = "INSERT INTO Details" +
            "  (firstname, lastname, age, DOB, gender, Degree, salary, phonenumber) VALUES " +
            " (?, ?, ?, ?, ?,?,?,?);";

        int result = 0;

        Class.forName("com.mysql.jdbc.Driver");
        try (Connection connection = DriverManager
            .getConnection("jdbc:mysql://localhost:3306/service?user=root&password=");
        		
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL)) {
            preparedStatement.setString(1, employee.getFirstname());
            preparedStatement.setString(2, employee.getLastname());
            preparedStatement.setString(3, employee.getAge());
            preparedStatement.setString(4, employee.getDOB());
            preparedStatement.setString(5, employee.getGender());
            preparedStatement.setString(6, employee.getDegree());
            preparedStatement.setString(7, employee.getSalary());
            preparedStatement.setString(8, employee.getPhonenumber());

            System.out.println(preparedStatement);
           
            result = preparedStatement.executeUpdate();

        } catch (SQLException e) {
           
            printSQLException(e);
        }
        return result;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
