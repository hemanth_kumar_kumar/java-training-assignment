package com.pack.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.springframework.stereotype.Component;

import jakarta.validation.constraints.Digits;
@Component
@Entity
@Table(name="CATALOG")
public class Catalog 
{
  @Override
	public String toString() {
		return "Catalog [productid=" + productid + ", productname=" + productname + ", product_brand=" + product_brand
				+ ", product_in_stock=" + product_in_stock + ", price=" + price + "]";
	}
@Id
  @Digits(integer=3,fraction=0,message="invalid product id")
  private int productid;
  
  @Column(columnDefinition="varchar(30)")
  @NotBlank(message="please fill the id")
  @Length(min=3,max=30,message="not matching the format length")
  private String productname;
  
  @Column(columnDefinition="varchar(30)")
  @NotBlank(message="please fill the id")
  @Length(min=3,max=30,message="not matching the format length")  
  private String product_brand;
  
  @Column(precision=3)
  @Digits(integer=3,fraction=0,message="exceeds the range")
  @Range(min=5,max=100,message="100 products maximum can take and minimum 5 products ")
  private int product_in_stock;
  
  @Column(precision=3,scale=2)
  @Digits(integer=3,fraction=2,message="exceeds the price limit")
  @Range(min=1,max=1000,message="price range between 1 to 100")
  private double price;
  
public int getProductid() {
	return productid;
}
public void setProductid(int productid) {
	this.productid = productid;
}
public String getProductname() {
	return productname;
}
public void setProductname(String productname) {
	this.productname = productname;
}
public String getProduct_brand() {
	return product_brand;
}
public void setProduct_brand(String product_brand) {
	this.product_brand = product_brand;
}
public int getProduct_in_stock() {
	return product_in_stock;
}
public void setProduct_in_stock(int product_in_stock) {
	this.product_in_stock = product_in_stock;
}
public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
  
}
