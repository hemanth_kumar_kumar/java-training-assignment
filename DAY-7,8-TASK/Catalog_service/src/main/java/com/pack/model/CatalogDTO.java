package com.pack.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
public class CatalogDTO{	 
	 @Override
	public String toString() {
		return "CatalogDTO [productid=" + productid + ", productname=" + productname + ", product_brand="
				+ product_brand + ", product_in_stock=" + product_in_stock + ", price=" + price + "]";
	}
	@JsonProperty("Product_NUMBER")
      private int productid;
	 @JsonProperty("PRODUCT_NAME")
	  private String productname;
	 @JsonProperty("BRAND_NAME")
	  private String product_brand;
	 @JsonProperty("AVAILABLE_IN_STOCK")
	  private int product_in_stock;
	 @JsonProperty("PRODUCT_COST")
	  private double price;
	 
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public String getProduct_brand() {
		return product_brand;
	}
	public void setProduct_brand(String product_brand) {
		this.product_brand = product_brand;
	}
	public int getProduct_in_stock() {
		return product_in_stock;
	}
	public void setProduct_in_stock(int product_in_stock) {
		this.product_in_stock = product_in_stock;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	  
}
