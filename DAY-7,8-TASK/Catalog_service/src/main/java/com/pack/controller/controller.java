package com.pack.controller;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pack.model.*;
import com.pack.service.Business;

@RestController
@RequestMapping("/catalog")
public class controller {
	
@Autowired
private Business BU;

@GetMapping(value="/products",produces=MediaType.APPLICATION_JSON)
public ResponseEntity allproducts() 
{
	 List<CatalogDTO> object= BU.totalcart();
	if(object.isEmpty())
	{
		return ResponseEntity.status(200).body("no value in DB");
	}
	
		return ResponseEntity.status(200).body(object);
}

@PostMapping(value="/products",produces=MediaType.APPLICATION_JSON,consumes=MediaType.APPLICATION_JSON)
public ResponseEntity saveproducts(@RequestBody CatalogDTO data)
{
	boolean status=BU.savedata(data);
	if(status) {return ResponseEntity.status(200).body("successfully saved in DB");}
	else
	{
		return ResponseEntity.status(200).body("already exist");
	}
}

@GetMapping(value="/product/{id}",produces=MediaType.APPLICATION_JSON)
public ResponseEntity geproductid(@PathVariable("id") int id)
{
	CatalogDTO dto= BU.getbyid(id);
	System.out.print(dto);
	if(dto==null)
	{
		return ResponseEntity.status(200).body("not in db");
	}
	return ResponseEntity.status(200).body(dto);

}
	
}
	

