package com.pack.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pack.model.Catalog;

@Repository
public interface repo extends CrudRepository<Catalog,Integer>
{

}
