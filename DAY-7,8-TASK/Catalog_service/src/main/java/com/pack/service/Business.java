package com.pack.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pack.data.repo;

import com.pack.model.*;

@Service
public class Business
{
  @Autowired
  private repo re;
  
  @Autowired
  private Catalog object;
  
  @Autowired
  private CatalogDTO instances;
  
  ModelMapper modelmapper=new ModelMapper();

public List<CatalogDTO> totalcart() {
	
	 List<Catalog> data=(List<Catalog>) re.findAll();
	 List<CatalogDTO>list=new ArrayList();
	 if(data.isEmpty())
	 {
		 return Arrays.asList(null);
	 }
	 else
	 {
		 for(Catalog val:data)
			{
				list.add(modelmapper.map(val,CatalogDTO.class));	
			}
		 return list;
	 }
	 
}

public CatalogDTO getbyid(int id) {
	Optional<Catalog> data=re.findById(id);
	if(data!=null)
	{
		System.out.print(data.get().toString());
        modelmapper.map(data.get(),instances);
    return instances;
	}
	else
	{
		return null;
	}
	
}

public boolean savedata(CatalogDTO data) {
	modelmapper.map(data,object);
	if(re.existsById(data.getProductid()))
	{
		return false;
	}
	else
	{
		re.save(object);
		return true;
	}
}
  

  
  
}
