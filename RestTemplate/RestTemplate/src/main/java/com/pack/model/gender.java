package com.pack.model;

import org.springframework.stereotype.Component;

@Component
public class gender 
{
 private String human_name;
 private String human_gender;
 private double chances;
 private int count;
public String getHuman_name() {
	return human_name;
}
public void setHuman_name(String human_name) {
	this.human_name = human_name;
}
public String getHuman_gender() {
	return human_gender;
}
public void setHuman_gender(String human_gender) {
	this.human_gender = human_gender;
}
public double getChances() {
	return chances;
}
public void setChances(double chances) {
	this.chances = chances;
}
public int getCount() {
	return count;
}
public void setCount(int count) {
	this.count = count;
}
public gender(String human_name, String human_gender, double chances, int count) {
	super();
	this.human_name = human_name;
	this.human_gender = human_gender;
	this.chances = chances;
	this.count = count;
}
public gender() {
	super();
	// TODO Auto-generated constructor stub
}
 
}
