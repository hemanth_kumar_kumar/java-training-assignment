package com.pack.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pack.model.gender;

@Service
public class Business
{
	private static final String get_all="http://universities.hipolabs.com/search?name=";
	private static final String get_postal="http://api.zippopotam.us/us/";
	private static final String get_gender="https://api.genderize.io?name=";
	private static  RestTemplate temp=new RestTemplate();

	public List<Object> getdatas(String name)
	{
		
		ResponseEntity<List> value=temp.getForEntity(get_all+name,List.class);
		  List val=value.getBody();
		  return val;
	}
	
	public Object getdata(String code)
	{
		ResponseEntity<Object> value=temp.getForEntity(get_postal+code,Object.class);  
		Object val=value.getBody();
		  return val;
	}
	
	public gender getdetails(String name) throws JsonMappingException, JsonProcessingException
	{
		ResponseEntity<String> value=temp.getForEntity(get_gender+name,String.class);
		ObjectMapper mapper = new ObjectMapper();
		System.out.print(value.getBody());
		
		JsonNode root = mapper.readTree(value.getBody());
		root.forEach(data->System.out.print(data));
		
		JsonNode person_name= root.path("name");
		JsonNode person_gender= root.path("gender");
		JsonNode person_chances= root.path("probability");
		JsonNode person_count= root.path("count");
		
		gender object=new gender(person_name.asText(),person_gender.asText(),person_chances.asDouble(),person_count.asInt());
		
		return object;
		
	}
}
