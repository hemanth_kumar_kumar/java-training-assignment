package com.pack.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.pack.model.gender;
import com.pack.service.Business;

@RequestMapping("/DB")
@RestController
public class controller
{
	@Autowired
	private Business bu;
	@GetMapping(value="/search/university/{name}")
  public List<Object> getdetails(@PathVariable("name") String name)
  {
	  List<Object> data=bu.getdatas(name);
	  return data;
  }
	
	@GetMapping(value="/search/postal/{code}")
	public Object getpostaldatas(@PathVariable("code") String code)
	{
		Object data=bu.getdata(code);
		return data;
	}
	
	@GetMapping(value="/search/gender/{name}")
	public gender getgender(@PathVariable("name") String name) throws JsonMappingException, JsonProcessingException
	{
		gender data=bu.getdetails(name);
		return data;
	}
}
